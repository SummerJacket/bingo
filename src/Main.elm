port module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes as Attrs
import Html.Events as Events
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)
import List.Extra
import Random
import Random.List



---- PORTS ----


port storeBoardP : String -> Cmd msg


port createBoardP : String -> Cmd msg


port queryBoardP : (String -> msg) -> Sub msg


createBoard : Cmd msg
createBoard =
    [ "I need X. Aaaaaaa"
    , "Saying something is OP"
    , "Something about removing a mod"
    , "Complaining about lag"
    , "yooooooo"
    , "Something to do with RLCraft"
    , "huuuuuuuh"
    , "Something about being disconnected"
    , "no w"
    , "Why is this here"
    , "What is this"
    , "Something to do with mana"
    , "Is the server on"
    , "What are thoseee"
    , "Hey you guys wanna do something"
    , "My X is gone"
    , "Liang get in here"
    , "Stop"
    , "Laughing"
    , "Whatcha trying to do hereeee"
    , "Jeeeeesus Christ"
    , "Muted microphone without noticing"
    , "Shouting fUnK"
    , "Liang come here"
    ]
        |> List.map (\x -> { label = x, selected = False })
        |> Encode.list encodeBingoCell
        |> Encode.encode 0
        |> createBoardP


storeBoard : List BingoCell -> Cmd msg
storeBoard board =
    board
        |> Encode.list encodeBingoCell
        |> Encode.encode 0
        |> storeBoardP



---- MODEL ----


type alias BingoCell =
    { label : String
    , selected : Bool
    }


encodeBingoCell : BingoCell -> Value
encodeBingoCell cell =
    Encode.object
        [ ( "label", Encode.string cell.label )
        , ( "selected", Encode.bool cell.selected )
        ]


decodeBingoCell : Decoder BingoCell
decodeBingoCell =
    Decode.map2 BingoCell
        (Decode.field "label" Decode.string)
        (Decode.field "selected" Decode.bool)


type alias Model =
    { bingoBoard : List BingoCell }


init : ( Model, Cmd Msg )
init =
    ( { bingoBoard = [] }, createBoard )



---- UPDATE ----


type Msg
    = NoOp
    | Randomize
    | NewBoard (List BingoCell)
    | ToggleCell String
    | ClearSelected


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Randomize ->
            ( model
            , Random.generate NewBoard (Random.List.shuffle model.bingoBoard)
            )

        NewBoard bingoBoard ->
            ( { model | bingoBoard = bingoBoard }
            , storeBoard bingoBoard
            )

        ToggleCell label ->
            case List.Extra.findIndex (\cell -> cell.label == label) model.bingoBoard of
                Nothing ->
                    update NoOp model

                Just i ->
                    let
                        elem =
                            List.Extra.getAt i model.bingoBoard
                                |> Maybe.withDefault { label = "", selected = False }

                        newBoard =
                            List.Extra.updateAt i
                                (always { elem | selected = not elem.selected })
                                model.bingoBoard
                    in
                    ( { model | bingoBoard = newBoard }
                    , storeBoard newBoard
                    )

        ClearSelected ->
            ( { model
                | bingoBoard =
                    List.map (\x -> { x | selected = False }) model.bingoBoard
              }
            , Cmd.none
            )



---- VIEW ----


viewCell : BingoCell -> Html Msg
viewCell cell =
    div
        [ Attrs.style "width" "100px"
        , Attrs.style "height" "100px"
        , Attrs.style "padding" "0.4rem"
        , Attrs.style "border" "1px solid black"
        , Attrs.style "text-align" "center"
        , Attrs.style "display" "flex"
        , Attrs.style "align-items" "center"
        , Attrs.style "background-color" <|
            if cell.selected then
                "#d0d0f0"

            else
                "transparent"
        , Events.onClick (ToggleCell cell.label)
        ]
        [ div [ Attrs.style "width" "100%" ]
            [ text cell.label ]
        ]


viewRow : List BingoCell -> Html Msg
viewRow row =
    div
        [ Attrs.style "display" "flex"
        , Attrs.style "justify-content" "center"
        ]
        (List.map viewCell row)


view : Model -> Html Msg
view model =
    let
        ( left, right ) =
            List.Extra.splitAt (List.length model.bingoBoard // 2) model.bingoBoard

        grid =
            List.Extra.groupsOf 5 <|
                left
                    ++ [ { label = "FREE", selected = True } ]
                    ++ right
    in
    div
        [ Attrs.style "margin" "1rem"
        , Attrs.style "font-family" "'Roboto Condensed', sans-serif"
        ]
        [ div [] (List.map viewRow grid)
        , div [ Attrs.style "margin-bottom" "0.8rem" ]
            [ button
                [ Attrs.style "margin-right" "1rem"
                , Events.onClick Randomize
                ]
                [ text "Randomize Board" ]
            , button [ Events.onClick ClearSelected ] [ text "Clear Selected" ]
            ]
        ]



---- SUBSCRIPTIONS ----


subscriptions : Model -> Sub Msg
subscriptions model =
    queryBoardP handleBoardQuery


handleBoardQuery : String -> Msg
handleBoardQuery json =
    case Decode.decodeString (Decode.list decodeBingoCell) json of
        Ok res ->
            NewBoard res

        Err err ->
            NoOp



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }
