import "./main.css";
import { Elm } from "./Main.elm";
import * as serviceWorker from "./serviceWorker";

const app = Elm.Main.init({
  node: document.getElementById("root")
});

app.ports.createBoardP.subscribe(board => {
  if (!localStorage.getItem("board")) {
    localStorage.setItem("board", board);
    console.log("Initialized board");
  } else {
    console.log("Not initializing board. Board already exists.");
  }

  app.ports.queryBoardP.send(localStorage.getItem("board"));
});

app.ports.storeBoardP.subscribe(board => localStorage.setItem("board", board));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
